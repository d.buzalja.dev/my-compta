<?php

namespace App\Form;

use App\Entity\RowAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RowAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('create_at', DateType::class, [
                // this is actually the default format for single_text
                'format' => 'dd-MM-yyyy',
            ])
            ->add('brand')
            ->add('amount')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RowAccount::class,
        ]);
    }
}
