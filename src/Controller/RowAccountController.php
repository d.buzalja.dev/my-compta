<?php

namespace App\Controller;

use App\Entity\RowAccount;
use App\Form\RowAccountType;
use App\Repository\RowAccountRepository;
use Container2LcYC81\getTemplateControllerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/row/account')]
class RowAccountController extends AbstractController
{
    #[Route('/', name: 'app_row_account_index', methods: ['GET'])]
    public function index(RowAccountRepository $rowAccountRepository): Response
    {
        $rows = $rowAccountRepository->findAll();
        $res = [];
        $item = "";
        $total = 0;
        $totaux = 0;

        foreach ($rows as $k => $row) {

            $rowMonth = date_format($row->getCreateAt(), 'M');
            if ($rowMonth != $item && $item != "") {
                $res[] = $total;
                $total = 0;
            }
            $total += $row->getAmount();
            $res[] = $row;
            $item = $rowMonth;

        }
        $res[] = $total;

        return $this->render('row_account/index.html.twig', [
            'row_accounts' => $res,
        ]);
    }

    #[Route('/new', name: 'app_row_account_new', methods: ['GET', 'POST'])]
    public function new(Request $request, RowAccountRepository $rowAccountRepository): Response
    {
        $rowAccount = new RowAccount();
        $form = $this->createForm(RowAccountType::class, $rowAccount);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rowAccountRepository->save($rowAccount, true);

            return $this->redirectToRoute('app_row_account_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('row_account/new.html.twig', [
            'row_account' => $rowAccount,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_row_account_show', methods: ['GET'])]
    public function show(RowAccount $rowAccount): Response
    {
        return $this->render('row_account/show.html.twig', [
            'row_account' => $rowAccount,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_row_account_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RowAccount $rowAccount, RowAccountRepository $rowAccountRepository): Response
    {
        $form = $this->createForm(RowAccountType::class, $rowAccount);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rowAccountRepository->save($rowAccount, true);

            return $this->redirectToRoute('app_row_account_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('row_account/edit.html.twig', [
            'row_account' => $rowAccount,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_row_account_delete', methods: ['POST'])]
    public function delete(Request $request, RowAccount $rowAccount, RowAccountRepository $rowAccountRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $rowAccount->getId(), $request->request->get('_token'))) {
            $rowAccountRepository->remove($rowAccount, true);
        }

        return $this->redirectToRoute('app_row_account_index', [], Response::HTTP_SEE_OTHER);
    }
}
